<?php


namespace App\Import\Buckets;


use App\Import\Buckets\Traits\Exportable;
use App\Import\Buckets\Traits\Importable;
use App\Import\Elements\Address;
use App\Import\Elements\OpeningHours;
use App\Import\Interfaces\Bucket;
use App\Import\Row;
use App\Models\ResShop;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

/**
 * Class NewShops
 * @package App\Import\Buckets
 * @property array markedRows
 * @property array headings
 * @property array importantColumns
 */
class NewShops implements Bucket, ToArray, WithHeadingRow
{
    use Exportable;

    use Importable;

    protected $markedRows;

    protected $importantColumns;

    protected $headings;

    /**
     * NewShops constructor.
     * @param bool $dryRun
     */
    public function __construct(bool $dryRun = true)
    {
        $this->markedRows = [];

        $this->importantColumns = [];

        $this->headings = Row::headings();

        $this->dryRun   = $dryRun;
    }

    /**
     *
     * part of check procedure
     * @param array $row
     * @param bool $alreadyMarked
     * @return bool
     */
    public function checkRow(array $row, bool $alreadyMarked): bool
    {
        if ($alreadyMarked) {
            return false;
        } else {
            if ($shop = ResShop::where(ResShop::SAP_NUMBER, $row[ROW::SAP_ACCOUNT_NUMBER])->first()) {
            } else {
                $this->addRow($row);
                return $this->remove();
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function remove(): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function title()
    {
        return "Nowe salony";
    }

    /**
     * @param $row
     * @return bool
     */
    public function addRow($row)
    {
        $this->markedRows[] = $row;
        return true;
    }

    /**
     * @return array
     */
    public function markedRows(): array
    {
        return $this->markedRows;
    }

    /**
     * @return array
     */
    public function headings()
    {
        return $this->headings;
    }

    /**
     * @return int
     */
    public function headingRow(): int
    {
        return 1;
    }

    /**
     * part of import procedure
     * @param array $array
     */
    public function array(array $array)
    {
        foreach ($array as $row) {
            if (!$this->dryRun) {
                /** @var ResShop $shop */
                $shop = ResShop::firstOrCreate(
                    [
                        ResShop::SAP_NUMBER => $row[Row::SAP_ACCOUNT_NUMBER],
                    ],
                    [
                        ResShop::GROUP_SAP_NUMBER  => $row[Row::LOCAL_CUSTOMER_GROUP],
                        ResShop::PRACTICE_NUMBER   => $row[Row::PRACTICE_NUMBER],
                        ResShop::NAME              => $row[Row::DISPLAY_NAME],
                        ResShop::PHONE             => $row[Row::PRACTICE_PHONE],
                        ResShop::ADDRESS           => Address::import($row[Row::SHIPPING_ADDRESS]),
                        ResShop::ACTIVE            => true,
                        ResShop::HIDE              => false,
                        ResShop::ACTIVATE_AT       => null,
                        ResShop::DEACTIVATE_AT     => null,
                        ResShop::LOYALTY           => false,
                        ResShop::OWNER_ID          => null,
                        ResShop::REPRESENTATIVE_ID => null,
                        ResShop::LONGITUDE         => $row[Row::GEOLOCATION_LONGITUDE],
                        ResShop::LATITUDE          => $row[Row::GEOLOCATION_LATITUDE],
                        ResShop::OPENING_HOURS     => OpeningHours::import($row),
                    ]);

            }
        }
    }
}